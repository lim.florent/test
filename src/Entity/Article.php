<?php

namespace App\Entity;

use App\Enum\ArticleStatus;

class Article
{
    private $id;

    /**
     * @var ArticleStatus
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ArticleStatus
     */
    public function getStatus(): ArticleStatus
    {
        return $this->status;
    }

    /**
     * @param ArticleStatus $status
     */
    public function setStatus(ArticleStatus $status): void
    {
        $this->status = $status;
    }
}
