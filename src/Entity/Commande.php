<?php

namespace App\Entity;

use App\Enum\CommandeStatus;

class Commande
{
    private $id;

    /**
     * @var CommandeStatus
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return CommandeStatus
     */
    public function getStatus(): CommandeStatus
    {
        return $this->status;
    }

    /**
     * @param CommandeStatus $status
     */
    public function setStatus(CommandeStatus $status): void
    {
        $this->status = $status;
    }
}
