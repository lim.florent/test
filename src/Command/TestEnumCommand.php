<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Commande;
use App\Enum\ArticleStatus;
use App\Enum\CommandeStatus;
use App\Enum\Status;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestEnumCommand extends Command
{
    protected static $defaultName = 'test:enum';
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    private function existant() {
        /*
         * UTILISATION ACTUELLE
         *
         * App\Common\Enum\AbstractEnum
         *
         * App\Contrat\Domain\Enum\StatutCommissionEnum extends AbstractEnum
         * App\Contrat\Domain\Commission\Statut
         *
         *
         * COTÉ INFRASTRUCTURE
         *
         * # Commission.Commission.orm.xml
         *  <embedded name="statut" class="App\Contrat\Domain\Commission\Statut"/>
         *
         * # Commission.Statut.orm.xml
         *  <embeddable name="App\Contrat\Domain\Commission\Statut">
         *      <field name="statut" type="string" length="155"/>
         *  </embeddable>
         *
         * Nom du champ : statut_statut
         */
    }

    /**
     * https://github.com/myclabs/php-enum
     * https://github.com/acelaya/doctrine-enum-type
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $article = new Article();

        $article->setStatus(ArticleStatus::DRAFT()); // Sans Type-hint
        $article->setStatus(ArticleStatus::REVIEWED()); // Avec Type-hint Annotation
        $article->setStatus(ArticleStatus::PUBLISHED()); // Avec Type-hint Methode


        $order = new Commande();
        $order->setStatus(CommandeStatus::COMPLETED());


        echo PHP_EOL;
        dump("Article", $article);
        dump("Array", ArticleStatus::toArray());

        echo PHP_EOL;
        dump("Order", $order);
        dump("Array", CommandeStatus::toArray());

        $this->em->persist($article);
        $this->em->persist($order);
        dump("Retrieved article published", $this->getArticleByStatus(ArticleStatus::PUBLISHED()));
        dump("Retrieved article draft", $this->getArticleByStatus(ArticleStatus::DRAFT()));
        dump("Retrieved order published", $this->getOrderByStatus(CommandeStatus::APPLIED()));
        dump("Retrieved order draft", $this->getOrderByStatus(CommandeStatus::COMPLETED()));
        $this->em->flush();
        return 0;
    }

    private function getArticleByStatus(ArticleStatus $status): ?Article {
        return $this->em->getRepository(Article::class)->findOneByStatus($status);
    }

    private function getOrderByStatus(CommandeStatus $status): ?Commande {
        return $this->em->getRepository(Commande::class)->findOneByStatus($status);
    }
}
