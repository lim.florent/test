<?php

namespace App\Command;

use App\Entity\Article;
use App\Enum\ArticleStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestDoctrineTransactionCommand extends Command
{
    protected static $defaultName = 'test:doctrine:transaction';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }
    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->clearEntities();

        $article = new Article();
        $article->setStatus(ArticleStatus::PUBLISHED());
        $this->em->persist($article);
        $this->em->flush();

        $this->em->getConnection()->beginTransaction();
            $article = $this->em->getRepository(Article::class)->findOneByStatus(ArticleStatus::PUBLISHED());
            $article->setStatus(ArticleStatus::REVIEWED());
            $this->em->persist($article);
            $this->em->flush();
        $this->em->getConnection()->rollBack();

        return 0;
    }

    public function clearEntities() {
        $repository = $this->em->getRepository(Article::class);

        $articles = $repository->findAll();
        foreach ($articles as $article) {
            $this->em->remove($article);
        }
        $this->em->flush();
    }
}
