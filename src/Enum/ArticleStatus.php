<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static ArticleStatus REVIEWED()
 */
class ArticleStatus extends Enum
{
    private const UNDEFINED = 'undefined';
    private const DRAFT = 'draft';
    private const REVIEWED = 'reviewed';
    private const PUBLISHED = 'published';

    /**
     * @return ArticleStatus
     */
    public static function PUBLISHED() {
        return new ArticleStatus(self::PUBLISHED);
    }

}