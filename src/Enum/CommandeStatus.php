<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static CommandeStatus SAVED()
 */
class CommandeStatus extends Enum
{
    private const UNDEFINED = 'undefined';
    private const SAVED = 'saved';
    private const APPLIED = 'applied';
    private const COMPLETED = 'completed';

    /**
     * @return CommandeStatus
     */
    public static function COMPLETED() {
        return new CommandeStatus(self::COMPLETED);
    }

}